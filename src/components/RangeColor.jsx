import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateColorPositon } from "../features/gradient";

export default function RangeColor() {
	const gradientValues = useSelector((state) => state.gradient);
	const dispatch = useDispatch();
	return (
		<input
			value={gradientValues.colors.find((color) => color.id === gradientValues.pickedColorId).position}
			onChange={(e) => dispatch(updateColorPositon(e.target.value))}
			className="w-full h-1 mb-10 bg-gray-200 rounded-lg cursor-pointer"
			type="range"
		/>
	);
}
